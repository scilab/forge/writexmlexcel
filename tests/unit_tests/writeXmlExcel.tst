// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
tlbxs = atomsGetInstalled();
if grep(tlbxs, 'WriteXmlExcel') <> [] then
  atomsLoad("WriteXmlExcel");
else
  root_path = getenv('TOOLBOX_WRITEXMLEXCEL_PATH', '');
  if root_path <> '' then 
    exec(root_path + 'loader.sce'); 
  end 
end
// ====================================================================
M = eye(4,4);
writeXmlExcel(TMPDIR + "/eye_matrix.xml", M);
// ====================================================================
M = [%nan, -%inf; 1, %inf];
writeXmlExcel(TMPDIR + "/naninf_matrix.xml", M);
// ====================================================================
M = ['Hello' 'writeXmlExcel'; 'Allan CORNET', '2010'];
writeXmlExcel(TMPDIR + "/string_matrix.xml", M);
// ====================================================================
M = [%t, %f; %f, %t];
writeXmlExcel(TMPDIR + "/boolean_matrix.xml", M);
// ====================================================================
